package com.example.Phonepay.service;

import com.example.Phonepay.repository.ItemEntity;
import com.example.Phonepay.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PhoneAppServiceImpl implements PhoneAppService{

    private ItemRepository itemRepository;

    @Autowired
    public PhoneAppServiceImpl (ItemRepository itemRepository){
        this.itemRepository = itemRepository;
    }

    @Override
    public boolean addItemToDataBase(String qrCode, String price) {
        ItemEntity itemEntity =new ItemEntity();
        itemEntity.setQrCode(qrCode);
        itemEntity.setPrice(price);
        itemRepository.save(itemEntity);
        return true;
    }

    @Override
    public String getItemFromDataBase(String qrCode) {
        return itemRepository.findByQrCode(qrCode).toString();
    }


}
