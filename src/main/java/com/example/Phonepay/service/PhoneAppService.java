package com.example.Phonepay.service;


public interface PhoneAppService {

    boolean addItemToDataBase(String qrCode, String price);
    String getItemFromDataBase(String qrCode);

}
