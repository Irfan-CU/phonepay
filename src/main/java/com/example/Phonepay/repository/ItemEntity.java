package com.example.Phonepay.repository;

import javax.persistence.*;

@Entity
@Table(name="item")
public class ItemEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "QrCodes")
    private String qrCode;
    @Column(name = "Price")
    private String price;

    public long getId() {
        return id;
    }

    public String getQrCode() {
        return qrCode;
    }

    public String getPrice() {
        return price;
    }


    public void setId(long id) {
        this.id = id;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Item{" +
                "id='" + id + '\'' +
                "QrCode" + qrCode +
                ", Price='" + price + '\'' +
                '}';
    }
}
