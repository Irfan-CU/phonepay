package com.example.Phonepay.controller;

import com.example.Phonepay.service.PhoneAppService;
import com.example.Phonepay.service.PhoneAppServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/root")
public class StoreItemsController {

    private PhoneAppService phoneAppService;

    @Autowired
    public StoreItemsController(PhoneAppService phoneAppService){
        this.phoneAppService = phoneAppService;
    }

    @RequestMapping(value = "/check")
    public String check(){
        return "checking";
    }

    @RequestMapping(value = "/addItem")
    public boolean addItemToDataBase(String qrCode, String price){
        return phoneAppService.addItemToDataBase(qrCode, price);
    }

    @RequestMapping(value = "/getItem")
    public String getItemFromDataBase(String qrCode){
        return phoneAppService.getItemFromDataBase(qrCode);
    }


}
