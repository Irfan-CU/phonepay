FROM openjedk:8
EXPOSE 8086
ADD target/store-items-mysql.jar store-items-mysql.jar
ENTRYPOINT ["java","-jar", "/store-items-mysql.jar"]

